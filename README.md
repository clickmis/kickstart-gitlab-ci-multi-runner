# Kickstart

This will install a fresh Gitlab CI Runner, you just need to register it to Gitlab afterwards.

The Kickstart recipe is basically a minimal CentOS, with all the magic happening in ```%post``` section.

As always, please consider this as a template or proof of concept. You know best of all which things you want to get kickstarted ;)

You might be interested in [dynamic Kickstart files](https://gitlab.com/snippets/26997) too.

## PXE

Add to any PXE boot menu file:

```
LABEL yes-centos7
	MENU LABEL CentOS Runner - DISK WILL BE WIPED (7)
	KERNEL centos/7/vmlinuz
	APPEND initrd=centos/7/initrd.img inst.stage2=http://ftp.hosteurope.de/mirror/centos.org/7/os/x86_64/ inst.repo=http://ftp.hosteurope.de/mirror/centos.org/7/os/x86_64/ inst.ks=https://gitlab.com/morph027/kickstart-gitlab-ci-multi-runner/raw/master/gitlab-runner.cfg rd.noverifyssl --
	SYSAPPEND 2
```

Also add installer files to PXE server, if not already there (pxe folder might be different):

```
export PXE_FOLDER="/var/lib/tftpboot"
mkdir -p $PXE_FOLDER/centos/7
cd $PXE_FOLDER/centos/7
wget http://ftp.hosteurope.de/mirror/centos.org/7/os/x86_64/isolinux/vmlinuz
wget http://ftp.hosteurope.de/mirror/centos.org/7/os/x86_64/isolinux/initrd.img
```

## Credentials

When installation is finished, the root password is ```centos```. 

## Hostname

* You can provide the desired hostname while booting by appending ```HOSTNAME=foobar``` to the ```APPEND``` line (by hitting Tab).
* Or you just can set it afterwards: ```hostnamectl set-hostname gitlab-ci-docker-runner-X```
* Might use [Dynamic Kickstart](#dynamic-kickstart):

## Register

Just follow the instructions [here](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/).

### Example

most suitable, just a plain docker runner:

```bash
 export BASE_URL="https://my.gitlab.com"
 export RUNNER_TOKEN="..." # grab from https://my.gitlab.com/admin/runners
gitlab-ci-multi-runner register -u "$BASE_URL/ci" -r "RUNNER_TOKEN" --name "$HOSTNAME" --executor "docker"  --docker-image "alpine:latest"
```
